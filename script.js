'use strict';

const inputEmail = document.getElementById('email');
const emailRequired = document.getElementById('email_required');
const noteEmail = document.getElementById('note_email');
const labelEmail = document.getElementById('label_email');
const invalidEmail = document.getElementById('email_invalid');

const inputPassword = document.getElementById('password');
const passwordRequired = document.getElementById('password_required');
const notePassword = document.getElementById('note_password');
const labelPassword = document.getElementById('label_password');
const lengthPassword = document.getElementById('password_length');

const inputCheckbox = document.getElementById('checkbox').checked;
const inputCheckboxCustom = document.getElementById('custom_checkbox');
const checkboxRequired = document.getElementById('checkbox_required');
const checkboxLoginForm = document.getElementById('form__checkbox');
const noteCheckbox = document.getElementById('note_checkbox');

const button = document.querySelector('.button');

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

button.addEventListener('click', (event) => {
	emailRequired.style.display = 'none';
	invalidEmail.style.display = 'none';
	passwordRequired.style.display = 'none';
	lengthPassword.style.display = 'none';
	checkboxRequired.style.display = 'none';

	let emailBool = false;
	let passwBool = false;
	let checkBool = false;
	
	if (inputEmail.value === '' || validateEmail(inputEmail.value) === false) {
		inputEmail.style.border = "	2px solid red";
		noteEmail.style.color = 'red';
		labelEmail.style.color = 'red';
		if (inputEmail.value === '') {
			emailRequired.style.display = 'contents';
		} else if (validateEmail(inputEmail.value) === false) {
			invalidEmail.style.display = 'contents';
		}
		emailRequired.style.margin = 'auto auto auto 18px';
		emailBool = false;
	} else {
		inputEmail.style.border = "2px solid #787878";
		noteEmail.style.color = '#787878';
		labelEmail.style.color = '#787878';
		emailRequired.style.display = 'none';
		invalidEmail.style.display = 'none';
		emailBool = true;
	}

	if (inputPassword.value === '' || inputPassword.value.length < 8) {
		inputPassword.style.border = "2px solid red";
		if (inputPassword.value === '') {
			passwordRequired.style.display = 'contents';
		} else if (inputPassword.value.length < 8) {
			lengthPassword.style.display = 'contents';
		} 
		notePassword.style.color = 'red';
		labelPassword.style.color = 'red';
		checkboxLoginForm.style.margin = '6px 45px 0 45px';
		passwordRequired.style.margin = 'auto auto auto 18px';
		passwBool = false;
	} else {
		inputPassword.style.border = "2px solid #787878";
		passwordRequired.style.display = 'none';
		lengthPassword.style.displsy = 'none';
		notePassword.style.color = '#787878';
		labelPassword.style.color = '#787878'
		checkboxLoginForm.style.margin = '16px 45px 0 45px';
		passwBool = true;
	}

	if (document.getElementById('checkbox').checked === false) {
		inputCheckboxCustom.style.border = "2px solid red";
		noteCheckbox.style.color = 'red';
		checkboxRequired.style.display = 'contents';
		checkboxRequired.style.margin = 'auto auto auto 18px';
		checkBool = false;
	} else {
		inputCheckboxCustom.style.border = "2px solid #787878";
		noteCheckbox.style.color = '#787878';
		checkboxRequired.style.display = 'none';
		checkBool = true;
	}

	if (emailBool && passwBool && checkBool) {
		console.log(`Email:\t\t${inputEmail.value}\nPassword:\t${inputPassword.value}`)
	}
	
	event.preventDefault();
});